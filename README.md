# ms-helper module
Helper module for nodejs microservices.
Validate requests using schema objects

## Custom validators
Add custom validators (plug-ins for [validatejs](https://validatejs.org/))
to file `lib/val.ts`

## Middlewares
```
const msHelper = require("ms-helper");
let allActions = {
    GET: {
        "/test/": {
            "request": {
                "body": {}
            }
        }
    },
    POST: {},
    PUT: {}
};

let app = express();

...

app.use(msHelper.enableHal);
app.use(msHelper.checkAuthHeaders);
app.use(msHelper.getActionsRouter(allActions));
```

`enableHal` - add hal method to response object
`checkAuthHeaders` - check that request contents appId and clientId headers
`getActionsRouter(allActions)` - middleware to check or describe request params
in val.ts constraints format