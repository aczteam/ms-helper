import {Helper} from "./lib/Helper";
import {validate} from "./lib/validate";

export declare function enableHal(req: any, res: any, next: any): void;

export declare function enableResponseValidation(req: any, res: any, next: any): void;

export declare function checkAuthHeaders(req: any, res: any, next: any): void;

export declare function getActionsRouter(actions: any): any;

export declare function getHelpRouter(actions: any): any;

export declare function parseUniqueParamError(err, req, res, next);

export {Helper};
export {validate};

// https://www.typescriptlang.org/docs/handbook/declaration-merging.html
declare global {
    namespace Express {
        interface Response {
            addSelfLink(responseObject);

            hal(responseObject);

            validate(responseObject);
        }
    }

    namespace Express {
        interface Request {
            action: any;
        }
    }
}