"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const validate_1 = require("../lib/validate");
describe("Detailed sample how MS response should be validated ( /v1/users/:id )", () => {
    let rules = {
        "/v1/users/:id": {
            "request": {
                "info": "Omit in this test"
            },
            "response": {
                "id": {
                    "presence": true,
                    "numericality": {
                        "onlyInteger": true,
                        "greaterThan": 0,
                    },
                },
                "appId": {
                    "presence": true,
                },
                "clientId": {
                    "presence": true,
                },
                "firstName": {
                    "presence": true,
                    "format": {
                        "pattern": "[a-zA-Z0-9- ]+",
                        "flags": "i",
                        "message": "can only contain alphanumeric characters, spaces and dashes."
                    }
                },
                "lastName": {
                    "presence": true,
                    "format": {
                        "pattern": "[a-zA-Z0-9- ]+",
                        "flags": "i",
                        "message": "can only contain alphanumeric characters, spaces and dashes."
                    }
                },
                "email": {
                    "presence": true,
                    "email": true
                },
                "_embedded.groups": {
                    presence: true,
                    "arrayOfObjects": {
                        items: {
                            id: {
                                presence: true,
                                numericality: {
                                    onlyInteger: true,
                                    greaterThan: 0,
                                },
                            },
                            "name": {
                                "presence": true,
                                "format": {
                                    "pattern": "[a-zA-Z0-9- ]+",
                                    "flags": "i",
                                    "message": "can only contain alphanumeric characters, spaces and dashes."
                                }
                            },
                            "_links.self.href": {
                                "presence": true,
                                "format": {
                                    "pattern": "[a-zA-Z0-9-/_.]+",
                                    "flags": "i",
                                    "message": "should be url"
                                }
                            }
                        }
                    }
                }
            }
        }
    };
    let responseRules = rules["/v1/users/:id"].response;
    let response = {
        "id": 1,
        "appId": "cc-app",
        "clientId": "cc-client",
        "firstName": "John",
        "lastName": "Cena",
        "email": "john.cena@klassapp.com",
        "_embedded": {
            "groups": []
        }
    };
    test("Groups not an array", () => {
        let o = Object.assign({}, response);
        o._embedded.groups = {};
        let r = validate_1.validate(o, responseRules);
        expect(r).toEqual({
            "_embedded.groups": [" embedded groups should be an array"]
        });
    });
    test("Group element missing all required params", () => {
        let o = Object.assign({}, response);
        o._embedded.groups = [{}];
        let r = validate_1.validate(o, responseRules);
        expect(r).toEqual({
            "_embedded.groups": [
                {
                    "_links.self.href": [" links self href can't be blank"],
                    "id": ["Id can't be blank"],
                    "name": ["Name can't be blank"]
                }
            ]
        });
    });
    test("All fine, but one group element has negative id", () => {
        let o = Object.assign({}, response);
        o._embedded.groups = [
            {
                id: 1,
                name: "Name1",
                "_links": {
                    "self": {
                        "href": "/v1/groups/1"
                    }
                }
            },
            {
                id: -2,
                name: "Name2",
                "_links": {
                    "self": {
                        "href": "/v1/groups/2"
                    }
                }
            }
        ];
        let r = validate_1.validate(o, responseRules);
        expect(r).toEqual({
            "_embedded.groups": [{ "id": ["Id must be greater than 0"] }]
        });
    });
    test("All fine, but one group element has wrong self link (has spaces in url)", () => {
        let o = Object.assign({}, response);
        o._embedded.groups = [
            {
                id: 1,
                name: "Name1",
                "_links": {
                    "self": {
                        "href": "/v1/groups/1"
                    }
                }
            },
            {
                id: 2,
                name: "Name2",
                "_links": {
                    "self": {
                        "href": "/v1/groups/ 2"
                    }
                }
            }
        ];
        let r = validate_1.validate(o, responseRules);
        expect(r).toEqual({
            "_embedded.groups": [{ "_links.self.href": [" links self href should be url"] }]
        });
    });
});
