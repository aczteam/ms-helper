"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Helper_1 = require("../lib/Helper");
describe('Test Helper class', () => {
    test('findAction should find by simple route', () => {
        let actions = {
            GET: {
                "/test": {
                    request: { body: {} },
                }
            }
        };
        let { action, params } = Helper_1.Helper.findAction("/test", "GET", actions);
        expect(action.request.body).toBeDefined();
        expect(params).toEqual({});
    });
    test('findAction should find by route with params and ignore URL query', () => {
        let actions = {
            GET: {
                "/test/:id/add/:user_id": {
                    request: { body: {} },
                }
            }
        };
        let { action, params } = Helper_1.Helper.findAction("/test/34/add/bill?id=12&param=value", "GET", actions);
        expect(action.request.body).toBeDefined();
        expect(params).toEqual({ "id": "34", "user_id": "bill" });
    });
    test('validateRequestData should return success==false', () => {
        let requestData = {
            headers: {
                header1: "wewewe",
                header2: 55
            },
            body: {
                bodyParam: true
            }
        };
        let rules = {
            request: {
                headers: {
                    header1: {
                        presence: true,
                        "numericality": {
                            "onlyInteger": true,
                            "greaterThan": 0
                        }
                    }
                }
            }
        };
        let r = Helper_1.Helper.validateRequestData(requestData, rules);
        expect(r.success).toBe(false);
        expect(r.messages.headers.header1[0]).toBe("Header1 is not a number");
    });
    test('validateRequestData should return success==true', () => {
        let requestData = {
            headers: {
                header1: "wewewe",
                header2: 55
            },
            body: {
                bodyParam: true,
                bodyParam2: "A"
            }
        };
        let rules = {
            request: {
                headers: {
                    header1: {
                        presence: true,
                    },
                    header2: {
                        presence: true,
                        "numericality": {
                            "onlyInteger": true,
                            "greaterThan": 0
                        }
                    }
                },
                body: {
                    bodyParam: {
                        presence: true
                    },
                    bodyParam2: {
                        presence: true,
                        equalOneOf: ["A", "B", "C"]
                    }
                }
            }
        };
        let r = Helper_1.Helper.validateRequestData(requestData, rules);
        expect(r.success).toBe(true);
        expect(r.messages.headers).not.toBeDefined();
    });
    test("Helpers.getTotalPages should return total pages count", () => {
        let getTotalPages = Helper_1.Helper.getTotalPages;
        expect(getTotalPages(10, 5)).toBe(2);
        expect(getTotalPages(11, 5)).toBe(3);
        expect(getTotalPages(14, 5)).toBe(3);
        expect(getTotalPages(16, 5)).toBe(4);
        expect(getTotalPages(1, 5)).toBe(1);
        expect(getTotalPages(0, 5)).toBe(0);
    });
    test("Helpers.mysqlDate should return date in MySQL format", () => {
        expect(Helper_1.Helper.getMysqlDate().indexOf("-")).toBe(4);
        expect(Helper_1.Helper.getMysqlDate(new Date("2017-11-24T14:05:52.000Z"))).toBe("2017-11-24 14:05:52");
    });
    test("Helper.addPaginationLinksIfPossible should add links", () => {
        let object = {
            "total": 2,
            "pageCount": 1,
            "page": 1,
            "pageSize": 20
        };
        let o = Helper_1.Helper.addPaginationLinksIfPossible(object, "/items");
        expect(o).toEqual({
            total: 2,
            pageCount: 1,
            page: 1,
            pageSize: 20,
            _links: {
                next: { href: '/items?page=1' },
                previous: { href: '/items?page=1' },
                first: { href: '/items?page=1' },
                last: { href: '/items?page=1' }
            }
        });
    });
    test("Helper.addPaginationLinksIfPossible should add links", () => {
        let object = {
            "total": 2,
            "pageCount": 10,
            "page": 6,
            "pageSize": 20
        };
        let o = Helper_1.Helper.addPaginationLinksIfPossible(object, "/items", { pageSize: 10, someParam: "abc" });
        expect(o).toEqual({
            total: 2,
            pageCount: 10,
            page: 6,
            pageSize: 20,
            _links: {
                next: { href: '/items?pageSize=10&someParam=abc&page=7' },
                previous: { href: '/items?pageSize=10&someParam=abc&page=5' },
                first: { href: '/items?pageSize=10&someParam=abc&page=1' },
                last: { href: '/items?pageSize=10&someParam=abc&page=10' }
            }
        });
    });
    test("Helper.addSelfLink", () => {
        let o = Helper_1.Helper.addSelfLink({}, "/link");
        expect(o).toEqual({
            _links: { self: { href: '/link' } }
        });
    });
    test("Helper.addSelfLink with query params", () => {
        let o = Helper_1.Helper.addSelfLink({}, "/link", { param1: 1, param2: "value2" });
        expect(o).toEqual({
            "_links": { "self": { "href": "/link?param1=1&param2=value2" } }
        });
    });
});
