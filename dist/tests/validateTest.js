"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const validate_1 = require("../lib/validate");
describe("Test validator 'plug-ins'", () => {
    test("equalOneOf", () => {
        let rules = {
            type: {
                equalOneOf: ["A", "B", "C", 1]
            }
        };
        expect(validate_1.validate({ type: "A" }, rules)).not.toBeDefined();
        expect(validate_1.validate({ type: 1 }, rules)).not.toBeDefined();
        expect(validate_1.validate({ type: "D" }, rules).type).toBeDefined();
        expect(validate_1.validate({ type: 2 }, rules).type).toBeDefined();
    });
    test("datetime", () => {
        let rules = {
            startAt: {
                datetime: true
            }
        };
        expect(validate_1.validate({ startAt: 1 }, rules).startAt).toBeDefined();
        expect(validate_1.validate({ startAt: true }, rules).startAt).toBeDefined();
        expect(validate_1.validate({ startAt: "some word" }, rules).startAt).toBeDefined();
        expect(validate_1.validate({ startAt: "2017-12-32 08:06:51" }, rules).startAt).toBeDefined();
        expect(validate_1.validate({ startAt: "2017-12-08 08:06:51" }, rules)).not.toBeDefined();
    });
    test("array", () => {
        let rules = {
            list: {
                array: true
            },
            list2: {
                array: {
                    items: {
                        length: {
                            maximum: 10,
                            minimum: 3,
                        },
                        format: {
                            pattern: '[a-zA-Z0-9- ]+',
                            flags: 'i',
                            message: 'can only contain alphanumeric characters, spaces and dashes.',
                        },
                    }
                }
            }
        };
        expect(validate_1.validate({ list: 1 }, rules).list[0]).toBe("List should be an array");
        expect(validate_1.validate({ list: ["A", "B", 1, 2, 3] }, rules)).not.toBeDefined();
        expect(validate_1.validate({ list2: ["AA", "BBB"] }, rules).list2).toBeDefined();
        expect(validate_1.validate({ list2: ["AAA", "BBB"] }, rules)).not.toBeDefined();
    });
    test("arrayOfObjects", () => {
        let rules = {
            list: {
                arrayOfObjects: {
                    items: {
                        id: {
                            presence: true,
                            numericality: {
                                onlyInteger: true,
                                greaterThan: 0,
                            },
                        },
                        name: {
                            equalOneOf: ["one", "two", "three"]
                        }
                    }
                }
            },
        };
        let r;
        r = validate_1.validate({ list: 1 }, rules);
        expect(r.list[0]).toBe("List should be an array");
        r = validate_1.validate({ list: [{}, {}] }, rules);
        expect(r.list[0].id[0]).toBe("Id can't be blank");
        r = validate_1.validate({ list: [{ id: 22 }, {}] }, rules);
        expect(r.list[0].id[0]).toBe("Id can't be blank");
        r = validate_1.validate({ list: [{ id: 22 }, { id: 23 }] }, rules);
        expect(r).not.toBeDefined();
        r = validate_1.validate({ list: [{ id: 22 }, { id: -100 }] }, rules);
        expect(r.list[0].id[0]).toBe("Id must be greater than 0");
        r = validate_1.validate({ list: [{ id: 1 }, { id: 2, name: "Five" }] }, rules);
        expect(r.list[0].name[0]).toBe("Name should be one of: one, two, three");
        r = validate_1.validate({ list: [{ id: 1 }, { id: 2, name: "two" }] }, rules);
        expect(r).not.toBeDefined();
    });
});
