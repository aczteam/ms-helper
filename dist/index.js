"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Helper_1 = require("./lib/Helper");
exports.Helper = Helper_1.Helper;
const ActionsRouter_1 = require("./lib/ActionsRouter");
const HelpRouter_1 = require("./lib/HelpRouter");
const validate_1 = require("./lib/validate");
exports.validate = validate_1.validate;
function enableHal(req, res, next) {
    let url = req.originalUrl;
    res.hal = (object) => {
        res.json(Helper_1.Helper.addSelfLink(object, url));
    };
    res.addSelfLink = (object) => {
        return Helper_1.Helper.addSelfLink(object, url, res.query);
    };
    next();
}
exports.enableHal = enableHal;
function enableResponseValidation(req, res, next) {
    let url = req.originalUrl;
    let rules = null;
    if (req.action && req.action.response) {
        rules = req.action.response;
    }
    res.validate = (object) => {
        if (rules) {
            let result = validate_1.validate(object, rules);
            if (result) {
                res.status(500);
                res.json(result);
            }
        }
    };
    next();
}
exports.enableResponseValidation = enableResponseValidation;
function checkAuthHeaders(req, res, next) {
    if (req.query.describe) {
        next();
    }
    else if (!req.get('appId') || !req.get('clientId')) {
        let r = {
            success: false,
            messages: {
                authorization: 'Invalid or missing appId or clientId'
            }
        };
        res.status(401);
        res.hal(r);
    }
    else {
        next();
    }
}
exports.checkAuthHeaders = checkAuthHeaders;
function parseUniqueParamError(err, req, res, next) {
    if (err.code != "ER_DUP_ENTRY") {
        next(err);
        return;
    }
    let bodyRules = req.action.request.body;
    let paramName = null;
    for (let key in bodyRules) {
        if (bodyRules[key].unique === true) {
            paramName = key;
        }
    }
    if (paramName) {
        let r = {
            success: false,
            messages: {
                body: {
                    [paramName]: [
                        `${paramName} should be unique`
                    ]
                }
            }
        };
        res.status(400);
        res.hal(r);
        return;
    }
    next(err);
}
exports.parseUniqueParamError = parseUniqueParamError;
function getActionsRouter(actions) {
    let actionsRouter = new ActionsRouter_1.ActionsRouter(actions);
    return actionsRouter.getRouter();
}
exports.getActionsRouter = getActionsRouter;
function getHelpRouter(actions) {
    let helpRouter = new HelpRouter_1.HelpRouter(actions);
    return helpRouter.router;
}
exports.getHelpRouter = getHelpRouter;
