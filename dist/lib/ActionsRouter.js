"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const Helper_1 = require("./Helper");
class ActionsRouter {
    constructor(actions) {
        this.actions = actions;
        this.router = express.Router();
        this.router
            .use(this.addActionData.bind(this))
            .use(this.describe.bind(this))
            .use(this.validate.bind(this));
    }
    addActionData(req, res, next) {
        let { action, params } = Helper_1.Helper.findAction(req.originalUrl, req.method, this.actions);
        req.action = action;
        req._params = params;
        next();
    }
    describe(req, res, next) {
        if (!req.action) {
            let r = {
                message: "No action has been defined for this route.",
                success: false,
                method: req.method
            };
            res.status(400);
            res.hal(r);
            return;
        }
        if (req.query.describe) {
            res.status(203);
            res.hal(req.action);
            return;
        }
        next();
    }
    ;
    validate(req, res, next) {
        let requestData = {
            headers: req.headers,
            query: req.query,
            body: req.body,
            path: req._params
        };
        let validationResponse = Helper_1.Helper.validateRequestData(requestData, req.action);
        if (validationResponse.success) {
            next();
        }
        else {
            res.status(400);
            res.hal(validationResponse);
        }
    }
    ;
    getRouter() {
        return this.router;
    }
}
exports.ActionsRouter = ActionsRouter;
