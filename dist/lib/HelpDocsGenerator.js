"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class HelpDocsGenerator {
    constructor() {
    }
    generate(actions) {
        return this.toHTML(this.makeArray(actions));
    }
    makeArray(actions) {
        let arr = [];
        for (let method in actions) {
            let obj = actions[method];
            for (let route in obj) {
                let a = {
                    method: method,
                    url: route
                };
                arr.push(a);
            }
        }
        return arr;
    }
    toHTML(arr) {
        let column1 = 10;
        let column2 = 50;
        let html = "";
        let horizontalLine = `+${this.chars(column1 + 2, "-")}+${this.chars(column2 + 2, "-")}+`;
        html += horizontalLine + "\n";
        html += `| ${"Method"} ${this.spaces(column1 - "Method".length)}|  ${"Url"}${this.spaces(column2 - "Url".length)}|` + "\n";
        html += horizontalLine + "\n";
        for (let a of arr) {
            html += `| ${a.method} ${this.spaces(column1 - a.method.length)}|  ${a.url}${this.spaces(column2 - a.url.length)}|`;
            html += "\n";
        }
        html += horizontalLine + "\n";
        html = `<pre>${html}</pre>`;
        return html;
    }
    chars(n, char) {
        let s = "";
        for (let i = 0; i < n; i++) {
            s += char;
        }
        return s;
    }
    spaces(n) {
        return this.chars(n, " ");
    }
}
exports.HelpDocsGenerator = HelpDocsGenerator;
