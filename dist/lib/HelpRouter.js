"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const HelpDocsGenerator_1 = require("./HelpDocsGenerator");
class HelpRouter {
    constructor(actions) {
        this.actions = actions;
        this.docsGenerator = new HelpDocsGenerator_1.HelpDocsGenerator();
        this.router = express.Router();
        this.router
            .get("/", this.getIndex.bind(this));
    }
    getIndex(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            let html = this.docsGenerator.generate(this.actions);
            res.send(html);
            res.end();
        });
    }
}
exports.HelpRouter = HelpRouter;
