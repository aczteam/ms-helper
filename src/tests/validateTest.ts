import {validate} from "../lib/validate";

describe("Test validator 'plug-ins'", () => {

    test("equalOneOf", () => {
        let rules = {
            type: {
                equalOneOf: ["A", "B", "C", 1]
            }
        };

        expect(validate({type: "A"}, rules)).not.toBeDefined();
        expect(validate({type: 1}, rules)).not.toBeDefined();
        expect(validate({type: "D"}, rules).type).toBeDefined();
        expect(validate({type: 2}, rules).type).toBeDefined();
    });

    test("datetime", () => {
        let rules = {
            startAt: {
                datetime: true
            }
        };

        expect(validate({startAt: 1}, rules).startAt).toBeDefined();
        expect(validate({startAt: true}, rules).startAt).toBeDefined();
        expect(validate({startAt: "some word"}, rules).startAt).toBeDefined();
        // 32 December - wrong date
        expect(validate({startAt: "2017-12-32 08:06:51"}, rules).startAt).toBeDefined();
        expect(validate({startAt: "2017-12-08 08:06:51"}, rules)).not.toBeDefined();
    });

    test("array", () => {
        let rules = {
            list: {
                array: true
            },

            list2: {
                array: {
                    items: {
                        length: {
                            maximum: 10,
                            minimum: 3,
                        },

                        format: {
                            pattern: '[a-zA-Z0-9- ]+',
                            flags: 'i',
                            message: 'can only contain alphanumeric characters, spaces and dashes.',
                        },

                    }
                }
            }

        };


        expect(validate({list: 1}, rules).list[0]).toBe("List should be an array");
        expect(validate({list: ["A", "B", 1, 2, 3]}, rules)).not.toBeDefined();
        expect(validate({list2: ["AA", "BBB"]}, rules).list2).toBeDefined();
        expect(validate({list2: ["AAA", "BBB"]}, rules)).not.toBeDefined();
    });

    test("arrayOfObjects", () => {
        let rules = {
            list: {
                arrayOfObjects: {
                    items: {
                        id: {
                            presence: true,
                            numericality: {
                                onlyInteger: true,
                                greaterThan: 0,
                            },

                        },
                        name: {
                            equalOneOf: ["one", "two", "three"]
                        }
                    }
                }
            },
        };

        // {list: [{id: 1, name: "one"}]}

        let r;
        r = validate({list: 1}, rules);
        expect(r.list[0]).toBe("List should be an array");

        r = validate({list: [{}, {}]}, rules);
        expect(r.list[0].id[0]).toBe("Id can't be blank");

        r = validate({list: [{id: 22}, {}]}, rules);
        expect(r.list[0].id[0]).toBe("Id can't be blank");

        r = validate({list: [{id: 22}, {id: 23}]}, rules);
        expect(r).not.toBeDefined();

        r = validate({list: [{id: 22}, {id: -100}]}, rules);
        expect(r.list[0].id[0]).toBe("Id must be greater than 0");

        r = validate({list: [{id: 1}, {id: 2, name: "Five"}]}, rules);
        expect(r.list[0].name[0]).toBe("Name should be one of: one, two, three");

        r = validate({list: [{id: 1}, {id: 2, name: "two"}]}, rules);
        expect(r).not.toBeDefined();
    });

});