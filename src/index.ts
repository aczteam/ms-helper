import {Helper} from "./lib/Helper";
import {ActionsRouter} from "./lib/ActionsRouter";
import {HelpRouter} from "./lib/HelpRouter";
import {validate} from "./lib/validate";

export function enableHal(req, res, next) {
    let url = req.originalUrl;
    res.hal = (object) => {
        res.json(Helper.addSelfLink(object, url));
    };

    res.addSelfLink = (object) => {
        return Helper.addSelfLink(object, url, res.query);
    };

    next();
}

export function enableResponseValidation(req, res, next) {
    let url = req.originalUrl;
    let rules = null;
    if (req.action && req.action.response) {
        rules = req.action.response;
    }

    res.validate = (object) => {

        if (rules) {
            let result = validate(object, rules);
            if (result) {
                res.status(500);
                res.json(result);
            }
        }

    };
    next();
}

export function checkAuthHeaders(req, res, next) {
    if (req.query.describe) {
        next();
    }
    else if (!req.get('appId') || !req.get('clientId')) {
        let r = {
            success: false,
            messages: {
                authorization: 'Invalid or missing appId or clientId'
            }
        };
        res.status(401);
        res.hal(r);

    } else {
        next();
    }
}

export function parseUniqueParamError(err, req, res, next) {

    if (err.code != "ER_DUP_ENTRY") {
        next(err);
        return;
    }

    let bodyRules = req.action.request.body;
    let paramName: string | null = null;
    for (let key in bodyRules) {
        if (bodyRules[key].unique === true) {
            paramName = key;
        }
    }

    if (paramName) {
        let r = {
            success: false,
            messages: {
                body: {
                    [paramName]: [
                        `${paramName} should be unique`
                    ]
                }
            }
        };

        res.status(400);
        res.hal(r);
        return;
    }

    next(err);
}

export function getActionsRouter(actions) {
    let actionsRouter = new ActionsRouter(actions);
    return actionsRouter.getRouter();
}

export function getHelpRouter(actions) {
    let helpRouter = new HelpRouter(actions);
    return helpRouter.router;
}


export {Helper};
export {validate}