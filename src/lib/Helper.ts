import * as Route from "route-parser";
import {validate} from "./validate";

import * as querystring from "querystring";

const stringify = querystring.stringify;

export class Helper {

    static validateRequestData(request, action) {
        let isValid = true;

        let query = validate(request.query, action.request.querystring);
        let path = validate(request.path, action.request.path);
        let headers = validate(request.headers, action.request.headers);
        let body = validate(request.body, action.request.body);

        if (query || path || headers || body) {
            isValid = false;
        }

        return {
            success: isValid,
            messages: {
                body: body,
                path: path,
                headers: headers,
                queryString: query
            }
        };
    };

    static findAction(url, method, allActions) {

        if (url.indexOf("?") > 0) {
            url = url.split("?")[0]
        }

        let actions = allActions[method];
        let action = actions[url];
        let params = {};

        if (!action) {
            //find it by path
            for (let p in actions) {
                let route = new Route(p);
                let match = (route.match(url)); // { page: 7 }
                if (match !== false) {
                    for (let m in match) {
                        url = url.replace("/" + match[m], "/:" + m)
                    }

                    params = match;
                }
            }

            action = actions[url];
        }
        return {action, params};
    };


    static getTotalPages(totalItems, itemsPerPage) {
        return Math.floor((totalItems + itemsPerPage - 1) / itemsPerPage);
    }

    /**
     * MySQL date
     * @param {Date} [date] Optional date object
     * @returns {string}
     */
    static getMysqlDate(date?) {
        date = date || new Date();
        let tmp = date.toISOString().split('T');
        return `${tmp[0]} ${tmp[1].split(".")[0]}`;
    }

    static addSelfLink(oldObject, link, query?) {
        let o = Object.assign({}, oldObject);
        if (!o._links) {
            o._links = {};
        }
        if (!o._links.self) {
            o._links.self = {};
        }

        o._links.self.href = link;

        if (query) {
            let queryObject = Object.assign({}, query);
            o._links.self.href = `${link}?${stringify(queryObject)}`;
        }

        return o;
    }

    static addPaginationLinksIfPossible(oldObject, link, query?) {

        if (
            oldObject.total === undefined ||
            oldObject.pageCount === undefined ||
            oldObject.page === undefined ||
            oldObject.pageSize === undefined
        ) {
            console.log("Cant add pagination links. Missing required params");
            return oldObject;
        }

        let o = Object.assign({}, oldObject);

        o._links = o._links || {};
        o._links.next = o._links.next || {};
        o._links.previous = o._links.previous || {};
        o._links.first = o._links.first || {};
        o._links.last = o._links.last || {};

        let current = parseInt(o.page);
        let pageCount = parseInt(o.pageCount);

        let next = current + 1;
        if (next > pageCount) {
            next = pageCount;
        }

        if (next == 0) {
            next = 1;
        }

        let previous = current - 1;
        if (previous < 1) {
            previous = 1;
        }

        let last = pageCount;
        let first = 1;
        if (last == 0) {
            last = 1;
        }

        let firstQuery = Object.assign({}, query);
        firstQuery.page = first;

        let previousQuery = Object.assign({}, query);
        previousQuery.page = previous;

        let nextQuery = Object.assign({}, query);
        nextQuery.page = next;

        let lastQuery = Object.assign({}, query);
        lastQuery.page = last;

        o._links.first.href = `${link}?${stringify(firstQuery)}`;
        o._links.previous.href = `${link}?${stringify(previousQuery)}`;
        o._links.next.href = `${link}?${stringify(nextQuery)}`;
        o._links.last.href = `${link}?${stringify(lastQuery)}`;

        return o;
    }

    static parseInt(array) {
        return array.map((element) => {
            return parseInt(element);
        });
    }
}