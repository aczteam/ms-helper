export class HelpDocsGenerator {
    constructor() {

    }

    public generate(actions): string {
        return this.toHTML(this.makeArray(actions));
    }

    private makeArray(actions): Action[] {


        let arr: Action[] = [];

        for (let method in actions) {
            let obj = actions[method];

            for (let route in obj) {
                let a: Action =
                    {
                        method: method as any,
                        url: route
                    };

                arr.push(a);
            }
        }

        return arr;
    }

    private toHTML(arr: Action[]): string {
        let column1 = 10;
        let column2 = 50;
        let html = "";

        let horizontalLine = `+${this.chars(column1 + 2, "-")}+${this.chars(column2 + 2, "-")}+`;
        html += horizontalLine + "\n";
        html += `| ${"Method"} ${this.spaces(column1 - "Method".length)}|  ${"Url"}${this.spaces(column2 - "Url".length)}|` + "\n";
        html += horizontalLine + "\n";

        for (let a of arr) {
            html += `| ${a.method} ${this.spaces(column1 - a.method.length)}|  ${a.url}${this.spaces(column2 - a.url.length)}|`;
            html += "\n";
        }

        html += horizontalLine + "\n";

        html = `<pre>${html}</pre>`;

        return html;
    }

    private chars(n: number, char: string): string {
        let s = "";
        for (let i = 0; i < n; i++) {
            s += char;
        }

        return s;
    }

    private spaces(n: number) {
        return this.chars(n, " ");
    }
}

interface Action {
    method: "POST" | "GET" | "PUT" | "DELETE"
    url: string

}