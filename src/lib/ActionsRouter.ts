import * as express from "express"
import {Helper} from "./Helper";

export class ActionsRouter {

    private router;

    constructor(private actions) {
        this.router = express.Router();

        this.router
            .use(this.addActionData.bind(this))
            .use(this.describe.bind(this))
            .use(this.validate.bind(this));
    }

    private addActionData(req, res, next) {
        let {action, params} = Helper.findAction(req.originalUrl, req.method, this.actions);
        req.action = action;
        req._params = params;
        next();
    }

    private describe(req, res, next) {
        if (!req.action) {
            let r = {
                message: "No action has been defined for this route.",
                success: false,
                method: req.method
            };

            res.status(400);
            res.hal(r);
            return;
        }

        if (req.query.describe) {
            res.status(203);
            res.hal(req.action);
            return;
        }

        next();
    };

    private validate(req, res, next) {

        let requestData = {
            headers: req.headers,
            query: req.query,
            body: req.body,
            path: req._params
        };
        let validationResponse = Helper.validateRequestData(requestData, req.action);

        if (validationResponse.success) {
            next()
        }
        else {
            res.status(400);
            res.hal(validationResponse);
        }
    };

    public getRouter() {
        return this.router;
    }
}