import * as validate from "validate.js";

/**
 * Add custom validators here
 */

validate.validators.datetime = function (value, options, key, attributes) {

    if (!value) {
        return null;
    }

    let error = 'has wrong format. Should be "YYYY-MM-DD hh:mm:ss" same as MySQL DATETIME';

    if (!validate.isString(value)) {
        return error;
    }

    let matchResult = value.match(/^\d\d\d\d-(\d)?\d-(\d)?\d \d\d:\d\d:\d\d$/g);

    if (matchResult === null) {
        return error;
    }

    try {
        new Date(value).toISOString();
    } catch (err) {
        return 'is bad date. Check that month is less than 13 etc';
    }
    return null;
};

validate.validators.equalOneOf = function (value, options, key, attributes) {

    if (!value) {
        return null;
    }

    if (options.indexOf(value) === -1) {
        return "should be one of: " + options.join(", ");
    }

    return null;
};

validate.validators.array = function (value, options, key, attributes) {

    if (!value) {
        return null;
    }

    if (validate.isArray(value) === false) {
        return "should be an array";
    }

    if (options.items) {
        let items = options.items;
        for (let element of value) {
            let validationResult = validate.single(element, items);
            if (validationResult) {
                return "array element " + validationResult;
            }
        }
    }

    return null;
};

validate.validators.arrayOfObjects = function (value, options, key, attributes) {

    if (!value) {
        return null;
    }

    if (validate.isArray(value) === false) {
        return "should be an array";
    }

    if (options.items) {
        let items = options.items;
        for (let element of value) {
            let validationResult = validate(element, items);
            if (validationResult) {
                return validationResult;
            }
        }
    }

    return null;
};

// Empty validator. Use "info" for documentation
validate.validators.info = function () {
    return null;
};

// Empty validator. Used as flag to mark unique index in database
validate.validators.unique = function () {
    return null;
};

export {validate};