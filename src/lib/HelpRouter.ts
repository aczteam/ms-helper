import * as express from "express";
import {NextFunction, Request, Response, Router} from "express";
import {HelpDocsGenerator} from "./HelpDocsGenerator";

export class HelpRouter {

    public readonly router: Router;
    private docsGenerator = new HelpDocsGenerator();

    constructor(private actions) {
        this.router = express.Router();
        this.router
            .get("/", this.getIndex.bind(this))
    }

    private async getIndex(req: Request, res: Response, next: NextFunction) {
        let html = this.docsGenerator.generate(this.actions);
        res.send(html);
        res.end();
    }
}